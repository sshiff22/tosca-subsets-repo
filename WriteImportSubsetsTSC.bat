
SET TSC_FILE=ImportSubsets.tsc

echo // Write ImportSubsets.tsc File    // > %TSC_FILE%
echo //---------------------------------// >> %TSC_FILE%
echo.
echo // Checkout the execution list // >> %TSC_FILE%
echo task "Checkout Tree" >> %TSC_FILE%
echo.
REM // Replace '\' with '\\' in the path, this is only for Jenkins run: //
set added_slashes=%WORKSPACE:\=\\%
echo.
echo // Import a subset // >> %TSC_FILE%
echo task "Import Subset" >> %TSC_FILE%
echo "%added_slashes%\\Subsets\\%EXECUTION_LIST_NAME%.tsu" >> %TSC_FILE%
echo "%added_slashes%\\Reporting.tsu" >> %TSC_FILE%
echo "<EOL>" >> %TSC_FILE%
echo.
echo // This is where we navigate to the recently imported Execution List  // >> %TSC_FILE%
echo // Drag and Drop it into the TestEvents folder // >> %TSC_FILE%
echo // Rename the TestEvent // >> %TSC_FILE%
echo JumpTo =^>SUBPARTS:ExecutionList[(Name=?"%EXECUTION_LIST_NAME%")] >> %TSC_FILE%
echo Mark >> %TSC_FILE%
echo cn "/Execution/TestEvents" >> %TSC_FILE%
echo DropMarked >> %TSC_FILE%
echo cn "<New TestEvent>" >> %TSC_FILE%
echo task "Rename" "latest-execution-list" >> %TSC_FILE%
echo.
echo // Check all back to Tosca repository // >> %TSC_FILE%
echo CheckinAll >> %TSC_FILE%
echo.
echo // Exit TCShell // >> %TSC_FILE%
echo exit >> %TSC_FILE%
echo.
echo // Confirm exit // >> %TSC_FILE%
echo y >> %TSC_FILE%
