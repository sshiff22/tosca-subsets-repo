@ECHO off

REM RunToscaExportTasks.bat
REM -----------------------------------------------------

"%commander_home%\TCShell.exe" -workspace "C:\Tosca_Projects\Tosca_Workspaces\DEX Workspace Master\DEX Workspace Master.tws" -login "Tosca" "tosca" "C:\Users\Administrator\gitRepo\tosca-subsets-repo\ExportSubsets.tsc"

REM Move the exported subset from temp (defined in tcs script) to the workspace:

REM move c:\temp\CI_ExecutionList_API.tsu "%WORKSPACE%"