
SET TSC_FILE=EmailReportAndExportSubsets.tsc

echo // EmailReportAndExportSubsets.tsc // > %TSC_FILE%
echo //---------------------------------// >> %TSC_FILE%
echo.
echo // Update Workscae from repository // >> %TSC_FILE%
echo UpdateAll >> %TSC_FILE%
echo.
echo // Navigate to the Execution List to run and one node up (to point to the folder) // >> %TSC_FILE%
echo JumpTo =^>SUBPARTS:ExecutionList[(Name=?"%EXECUTION_LIST_NAME%")] >> %TSC_FILE%
echo cn ../ >> %TSC_FILE%
echo.
echo // generate report and save it in the WORKSPACE//  >> %TSC_FILE%
echo task "Print Report ... ExecutionEntries with detailed Logs" >> %TSC_FILE%
echo.
set added_slashes=%WORKSPACE:\=\\%
echo "%added_slashes%\\%EXECUTION_LIST_NAME%_RunReport.pdf" >> %TSC_FILE%
echo.
echo // Export the Subset // >> %TSC_FILE%
echo task "Export Subset" >> %TSC_FILE%
echo.
echo // Pick Export path // >> %TSC_FILE%
echo "%added_slashes%\\Subsets\\%EXECUTION_LIST_NAME%.tsu" >> %TSC_FILE%
echo.
echo // Send email Report >> %TSC_FILE%
echo // Navigate one levele up and search the 'Email Report' Execution List // >> %TSC_FILE%
echo cn ../ >> %TSC_FILE%
echo JumpTo =^>SUBPARTS:ExecutionList[(Name=?"Email\ Report")] >> %TSC_FILE%
echo.
echo // Checkout the execution list // >> %TSC_FILE%
echo task "Checkout Tree" >> %TSC_FILE%
echo.
echo task run >> %TSC_FILE%
echo.
echo // Check all back to Tosca repository // >> %TSC_FILE%
echo CheckinAll >> %TSC_FILE%
echo.
echo // Exit TCShell // >> %TSC_FILE%
echo exit >> %TSC_FILE%
echo.
echo // Confirm exit // >> %TSC_FILE%
echo y >> %TSC_FILE%
